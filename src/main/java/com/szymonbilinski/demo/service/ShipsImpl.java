package com.szymonbilinski.demo.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Stream;

@Service
public class ShipsImpl implements Ships {

    private static final Logger LOG = LoggerFactory.getLogger(ShipsImpl.class);
    String[] arr;

    public ShipsImpl() {
        this.arr = new String[]{
                " ",
                " ",
                "S",
                " ",
                " ",
                "S"
        };
    }


    @Override
    public int changeValue(int position) {
        try {
            if (arr[position].equals("S")) {
                arr[position] = "X";
                getArr();
                return 1;
            } else {
                arr[position] = "O";
                getArr();
                return 0;
            }
        } catch (IndexOutOfBoundsException e) {
            getArr();
            return 2;
        }
    }

    @Override
    public void getArr() {
        LOG.info("---plansza:"+Arrays.toString(arr));
    }
}
