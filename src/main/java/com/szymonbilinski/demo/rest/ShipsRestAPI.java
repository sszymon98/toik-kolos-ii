package com.szymonbilinski.demo.rest;

import com.szymonbilinski.demo.service.Ships;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ShipsRestAPI {
    final Ships ships;

    public ShipsRestAPI(Ships ships) {
        this.ships = ships;
    }

    @PostMapping("/api/shot")
    public ResponseEntity<?> shipPost(@RequestParam(name = "position") int index){
        int result = ships.changeValue(index);
        if (result == 1){
            return new ResponseEntity<>(HttpStatus.OK);
        }else if (result == 0){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

}
